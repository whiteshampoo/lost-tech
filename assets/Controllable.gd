extends Spatial
class_name Controllable

# warning-ignore:unused_signal
signal updated_output_1
# warning-ignore:unused_signal
signal updated_output_2
# warning-ignore:unused_signal
signal activated_output_1
# warning-ignore:unused_signal
signal activated_output_2
# warning-ignore:unused_signal
signal deactivated_output_1
# warning-ignore:unused_signal
signal deactivated_output_2

enum INPUT_TYPE {
	AND,
	NAND,
	OR,
	NOR,
	XOR,
	XNOR
}

export (INPUT_TYPE) var input_type_1 : int = 0
export var always_input_1 : bool = false
export var inverse_switch_1 : bool = false
export (Array, Resource) var inputs_1 : Array = Array()
export (INPUT_TYPE) var input_type_2 : int = 0
export var always_input_2 : bool = false
export var inverse_switch_2 : bool = false
export (Array, Resource) var inputs_2 : Array = Array()
export var sound : AudioStreamOGGVorbis = null

var input_1 : bool = false
var input_2 : bool = false
var output_1 : bool = false
var output_2 : bool = false
var switch_1 : bool = false setget set_switch_1
var switch_2 : bool = false setget set_switch_2

var audio : AudioStreamPlayer = AudioStreamPlayer.new()
var saving : Array = Array()

func set_switch_1(switch : bool) -> void:
	switch_1 = switch
	updating_1()


func set_switch_2(switch : bool) -> void:
	switch_2 = switch
	updating_2()
	

func _ready() -> void:
	audio.stream = sound
	audio.bus = "Sound"
	add_child(audio)
	register_input(1)
	register_input(2)
	saving.append("switch_1")
	saving.append("switch_2")
	saving.append("input_1")
	saving.append("input_2")
	saving.append("output_1")
	saving.append("output_2")
	prop_load()

func _exit_tree() -> void:
	prop_save()


func prop_save() -> void:
	return
#	for prop in saving:
#		prints("save", prop, get(prop))
#		Global.puzzle_save[name] = get(prop)


func prop_load() -> void:
	return
#		for prop in Global.puzzle_save:
#			prints("load", prop, Global.puzzle_save[prop])
#			set(prop, Global.puzzle_save[prop])


func register_input(suffix : int) -> void:
	assert(suffix in [1, 2])
	if get("always_input_%d" % suffix):
		set("input_%s" % suffix, true)
	else:
		for input in get("inputs_%d" % suffix):
# warning-ignore:return_value_discarded
			get_node(input.path).connect("updated_%s" % input.prop, self, "updating_%d" % suffix)
		#call("updating_%d" % suffix)


func play_sound() -> void:
	audio.stop()
	audio.play()


func xor(a : bool, b : bool) -> bool:
	return (a and not b) or (not a and b) 


func updating_1() -> void:
	input_1 = always_input_1 or updating(input_type_1, inputs_1)
	var out : bool = output_1
	output_1 = input_1 and xor(switch_1, inverse_switch_1)
	if out != output_1:
		call_deferred("emit_signal", "updated_output_1")
		if output_1:
			call_deferred("emit_signal", "activated_output_1")
		else:
			call_deferred("emit_signal", "deactivated_output_1")


func updating_2() -> void:
	input_2 = always_input_2 or updating(input_type_2, inputs_2)
	var out : bool = output_2
	output_2 = input_2 and xor(switch_2, inverse_switch_2)
	if out != output_2:
		call_deferred("emit_signal", "updated_output_2")
		if output_2:
			call_deferred("emit_signal", "activated_output_2")
		else:
			call_deferred("emit_signal", "deactivated_output_2")
			

func updating(input_type : int, inputs : Array) -> bool:
	var input : bool = false
	for i in inputs.size():
		var value : bool = get_node(inputs[i].path).get(inputs[i].prop)
		if not i: # First input
			input = value
			continue
		match input_type:
			INPUT_TYPE.AND, INPUT_TYPE.NAND:
				input = input and value
			INPUT_TYPE.OR, INPUT_TYPE.NOR:
				input = input or value
			INPUT_TYPE.XOR, INPUT_TYPE.XNOR:
				input = xor(input, value)
			_:
				assert(false, "Unknown logic-operator")
	if input_type in [INPUT_TYPE.NAND, INPUT_TYPE.NOR, INPUT_TYPE.XNOR]:
		input = not input
	return input
