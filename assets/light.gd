extends MeshInstance
class_name LED
tool

enum MODES {
	OFF,
	ON,
	ACTIVE
}

export (MODES) var mode : int = MODES.OFF setget set_mode
export var mat_off : Material = preload("res://assets/global_materials/plastic/Light_off.material")
export var mat_on : Material = preload("res://assets/global_materials/plastic/Light_on.material")
export var mat_active : Material = preload("res://assets/global_materials/plastic/Light_active.material")

var mat : Array = Array()
var light : OmniLight = null

func _get_configuration_warning() -> String:
	var childs : Array = get_children()
	if childs.size() != 1:
		return "A light should have ONE child of type OmniLight"
	if not childs[0] is OmniLight:
		return "Child is not an OmniLight"
	return ""
	

func _ready() -> void:
	setup()


func setup() -> void:
	mat.clear()
	mat.append(mat_off)
	mat.append(mat_on)
	mat.append(mat_active)
	
	var childs : Array = get_children()
	if childs.size() and childs[0] is OmniLight:
		light = childs[0]


func set_mode(m : int) -> void:
	if not mat:
		setup()
	mode = m
	set_surface_material(0, mat[mode])
	if is_instance_valid(light):
		light.light_color = mat[mode].emission
		light.visible = mode > 0
