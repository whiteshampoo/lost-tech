extends Controllable

onready var led1 : LED = $LED1
onready var led2 : LED = $LED2

onready var Anim : AnimationPlayer = $Anim


func _ready() -> void:
	switch_1 = false
	switch_2 = true
	updating_1()
	updating_2()


func set_led(input : bool, switch : bool, led : LED) -> void:
	if input:
		if switch:
			led.set_mode(LED.MODES.ACTIVE)
		else:
			led.set_mode(LED.MODES.ON)
	else:
		led.set_mode(LED.MODES.OFF)
		

func updating_1() -> void:
	.updating_1()
	set_led(input_1, switch_1, led1)


func updating_2() -> void:
	.updating_2()
	set_led(input_2, switch_2, led2)
	

func switch() -> void:
	if switch_1:
		Anim.play_backwards("activate")
	else:
		Anim.play("activate")
	set_switch_1(not switch_1)
	set_switch_2(not switch_2)


func _on_Area_input_event(_camera: Node, event: InputEvent, _click_position: Vector3, _click_normal: Vector3, _shape_idx: int) -> void:
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		play_sound()
		switch()
