extends Controllable

onready var Fuse1 : MeshInstance = $Fuse
onready var Fuse2 : MeshInstance = $FuseDekoration

export var fused : bool = false

func _ready() -> void:
	print(owner.name)
	fuse(fused)

func fuse(a : bool) -> void:
	fused = a
	switch_1 = a
	switch_2 = false
	Fuse1.visible = a
	Fuse2.visible = a
	updating_1()

func _on_Area_input_event(_camera: Node, event: InputEvent, _click_position: Vector3, _click_normal: Vector3, _shape_idx: int) -> void:
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		if Global.inventory["Fuse"] > 0 and not fused:
			play_sound()
			Global.inventory["Fuse"] -= 1
			fuse(true)
		elif fused:
			play_sound()
			Global.inventory["Fuse"] += 1
			fuse(false)
