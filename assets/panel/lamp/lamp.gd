extends Controllable

onready var led : LED = $Light


func _ready() -> void:
	switch_1 = true
	switch_2 = true
	updating_1()

func set_led() -> void:
	if input_2:
		led.set_mode(LED.MODES.ACTIVE)
	elif input_1:
		led.set_mode(LED.MODES.ON)
	else:
		led.set_mode(LED.MODES.OFF)


func updating_1() -> void:
	.updating_1()
	set_led()


func updating_2() -> void:
	.updating_2()
	if input_2:
		always_input_2 = true
		always_input_1 = true
	set_led()

