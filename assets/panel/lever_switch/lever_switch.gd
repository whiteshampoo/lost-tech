extends Controllable


onready var Anim : AnimationPlayer = $AnimationPlayer


func _ready() -> void:
	updating_1()
	updating_2()
	

func switch() -> void:
	if switch_1:
		Anim.play_backwards("activate")
	else:
		Anim.play("activate")
	set_switch_1(not switch_1)
	set_switch_2(not switch_2)
	
	


func _on_Area_input_event(_camera: Node, event: InputEvent, _click_position: Vector3, _click_normal: Vector3, _shape_idx: int) -> void:
	if Anim.is_playing():
		return
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		play_sound()
		switch()
