extends Controllable

onready var Anim : AnimationPlayer = $AnimationPlayer
onready var led : LED = $Button

func _ready() -> void:
	switch_1 = true
	switch_2 = false
	updating_1()
	updating_2()
	if input_1:
		enable()
		Anim.seek(Anim.current_animation_length, true)

func disable() -> void:
	if not Anim.is_playing():
		Anim.play_backwards("Open")
	else:
		#push_warning("Animation interrupted (FIXME)")
		Anim.play_backwards("Open")
		Anim.seek(0.0)


func enable() -> void:
	if not Anim.is_playing():
		Anim.play("Open")
	else:
		#push_warning("Animation interrupted (FIXME)")
		Anim.play("Open")
		Anim.seek(Anim.current_animation_length)


func updating_1() -> void:
	var input : bool = input_1
	.updating_1()
	if input == input_1:
		return
	if input_1:
		enable()
	else:
		disable()

func updating_2() -> void:
	.updating_2()
	switch(true)

func switch(dry_run : bool = false) -> void:
	if not dry_run and input_2:
		switch_2 = not switch_2
	.updating_2()
	if output_2:
		led.set_mode(LED.MODES.ACTIVE)
	elif input_2:
		led.set_mode(LED.MODES.ON)
	else:
		led.set_mode(LED.MODES.OFF)

func _on_Area_input_event(_camera: Node, event: InputEvent, _click_position: Vector3, _click_normal: Vector3, _shape_idx: int) -> void:
	if Anim.is_playing(): return
	if not input_1: return
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		Anim.play("PushButton")
		play_sound()
		switch()
