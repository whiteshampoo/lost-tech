extends Controllable

onready var led : LED = $Signal


func _ready() -> void:
	switch_1 = true
	switch_2 = true
	

func set_led() -> void:
	output_1 = input_1 and input_2
	output_2 = input_1 and input_2
	prints(output_1, output_2)
	if xor(input_1, input_2):
		led.set_mode(LED.MODES.ON)
	elif input_1 and input_2:
		led.set_mode(LED.MODES.ACTIVE)
	else:
		led.set_mode(LED.MODES.OFF)
	emit_signal("updated_output_1")
	emit_signal("updated_output_2")


func updating_1() -> void:
	.updating_1()
	set_led()


func updating_2() -> void:
	.updating_2()
	set_led()
