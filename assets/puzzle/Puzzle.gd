extends Controllable

onready var CameraPivot : Spatial = $CameraPivot
onready var Cam : Camera = $CameraPivot/Camera
onready var CameraZoom : Tween = $CameraZoom

export var fuses : int = 0

func _ready() -> void:
	Global.inventory["Fuse"] = fuses

func _input(event: InputEvent) -> void:
	if not event is InputEventMouse:
		return
	var mouse : Vector2 = event.global_position
	var viewport : Vector2 = OS.window_size#get_viewport().get_visible_rect().size
	mouse /= viewport
	mouse -= Vector2.ONE / 2.0
	
	if event is InputEventMouseMotion:
		CameraPivot.rotation_degrees.y = -45.00 * mouse.x
		CameraPivot.rotation_degrees.x = 11.25 * mouse.y
		return
	
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT:
# warning-ignore:return_value_discarded
		CameraZoom.stop_all()
# warning-ignore:return_value_discarded
		CameraZoom.remove_all()
		if event.pressed:
# warning-ignore:return_value_discarded
			CameraZoom.interpolate_property(Cam, "fov", null, 25.0, 
											0.25, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
			CameraZoom.interpolate_property(Cam, "rotation_degrees:y", null, 
											-45.0 * mouse.x * 3.0, 
											0.25, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
			CameraZoom.interpolate_property(Cam, "rotation_degrees:x", null, 
											-11.25 * mouse.y * 5.0 - 30.0, 
											0.25, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		else:
# warning-ignore:return_value_discarded
			CameraZoom.interpolate_property(Cam, "fov", null, 70.0, 0.25, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
			CameraZoom.interpolate_property(Cam, "rotation_degrees:y", null, 0.0, 
											0.25, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
			CameraZoom.interpolate_property(Cam, "rotation_degrees:x", null, -30, 
											0.25, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
		CameraZoom.start()
		return


func updating_1() -> void:
	.updating_1()
	check_win()


func updating_2() -> void:
	.updating_2()
	check_win()


func check_win() -> void:
	prints(input_1, input_2)
	if input_1 and input_2:
		play_sound()
		$Finished.start()


func _on_Finished_timeout() -> void:
	Global.change_scene("res://ui/mainmenu/MainMenu.tscn")
