extends Node

enum {
	TRANS_NONE
}

const FILE : String = "user://settings.json"

export (Array, Resource) var music_files : Array = Array()

onready var MusicPlayer : AudioStreamPlayer = $MusicPlayer
onready var MusicVolume : Tween = $MusicVolume

var audio_bus_mute : Dictionary = Dictionary()
var puzzle_save : Dictionary = Dictionary()
var level : int = 0
var playing : int = 0

func save_settings() -> void:
	var file : File = File.new()
	if file.open(FILE, File.WRITE):
		push_error("Cannot open settings file for saving")
		return
	var dict : Dictionary = Dictionary()
	dict["sound_volume"] = Global.audio_get_volume("Sound")
	dict["sound_mute"] = Global.audio_get_mute("Sound")
	dict["music_volume"] = Global.audio_get_volume("Music")
	dict["music_mute"] = Global.audio_get_mute("Music")
	dict["fullscreen"] = OS.window_fullscreen
	dict["level"] = level
	file.store_string(JSON.print(dict, " "))
	file.close()


func load_settings() -> void:
	var file : File = File.new()
	if file.open(FILE, File.READ):
		save_settings()
		if file.open(FILE, File.READ):
			push_error("Cannot open settings file for loading")
			return
	var import : JSONParseResult = JSON.parse(file.get_as_text())
	if import.error:
		push_warning("Settings error: %s" % import.error_string)
		return
	var dict : Dictionary = import.result
	Global.audio_set_volume("Sound", dict.get("sound_volume", 1.0))
	Global.audio_set_mute("Sound", dict.get("sound_mute", false))
	Global.audio_set_volume("Music", dict.get("music_volume", 1.0))
	Global.audio_set_mute("Music", dict.get("music_mute", false))
	OS.window_fullscreen = dict.get("fullscreen", false)
	level = dict.get("level", 0)
	file.close()


func audio_get_bus(bus : String) -> int:
	var idx : int = AudioServer.get_bus_index(bus)
	if idx == -1:
		push_error("Audio-Bus '%s' not found" % bus)
	return idx
	
	
func audio_set_mute(bus : String, mute : bool) -> void:
	var idx : int = audio_get_bus(bus)
	if idx == -1:
		return
	audio_bus_mute[bus] = mute
	AudioServer.set_bus_mute(idx, mute)
	#get_tree().call_deferred("call_group", "VolumeSlider", "audio_update_mute", bus, mute, updater)
	get_tree().call_group("VolumeSlider", "audio_update_mute", bus, mute)
	
	
func audio_get_mute(bus : String) -> bool:
	return audio_bus_mute.get(bus, false)


func audio_set_volume(bus : String, linear : float) -> void:
	var idx : int = audio_get_bus(bus)
	if idx == -1:
		return
	linear = clamp(linear, 0.0, 1.0)
	if not is_equal_approx(linear, audio_get_volume(bus)):
		AudioServer.set_bus_volume_db(idx, linear2db(linear))
		#get_tree().call_deferred("call_group", "VolumeSlider", "audio_update_volume", bus, linear)
		get_tree().call_group("VolumeSlider", "audio_update_volume", bus, linear)


func audio_get_volume(bus : String) -> float:
	var idx : int = audio_get_bus(bus)
	if idx == -1:
		return 0.0
	return clamp(db2linear(AudioServer.get_bus_volume_db(idx)), 0.0, 1.0)
	

var inventory : Dictionary = Dictionary()

func _ready() -> void:
	OS.min_window_size = Vector2(1024, 600)
	randomize()
	inventory["Fuse"] = 0
	load_settings()
	play_music()
# warning-ignore:return_value_discarded
	MusicVolume.interpolate_property(MusicPlayer, "volume_db", 
									-80, 0.0, 5.0, 
									Tween.TRANS_CIRC, Tween.EASE_OUT)
# warning-ignore:return_value_discarded
	MusicVolume.start()


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("fullscreen"):
		OS.window_fullscreen = not OS.window_fullscreen
	if Input.is_action_just_pressed("next_song"):
		play_music()


func _exit_tree() -> void:
	save_settings()


func play_music() -> void:
	MusicPlayer.stop()
	MusicPlayer.stream = music_files[randi() % music_files.size()]
	MusicPlayer.play()
	

func change_scene(path : String, _transition : int = TRANS_NONE) -> void:
	var error : int = get_tree().change_scene(path)
	assert(not error, "Error: %d, path: %s" % [error, path])


func change_scene_to(packed_scene : PackedScene, _transition : int = TRANS_NONE) -> void:
	var error : int = get_tree().change_scene_to(packed_scene)
	assert(not error, error)
