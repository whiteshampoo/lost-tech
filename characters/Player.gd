extends KinematicBody2D
class_name Player

export(int) var forward_speed : int = 300
export(int) var sideways_speed : int = 200
export(int) var acceleration : int = 600
export(int) var deceleration : int = 1200
export(int) var look_speed : int = 10

onready var animation_tree : AnimationTree = $AnimationTree
onready var interaction_label : Label = $InteractionArea2D/Label
onready var interaction_label_pos : Position2D = $InteractionArea2D/LabelPosition2D

var idle_walk : AnimationNodeStateMachinePlayback = null

var velocity : Vector2 = Vector2.ZERO
var look_angle : float = 0.0
var interactable : Node = null

func _ready():
	idle_walk = animation_tree['parameters/idle_walk/playback']
	interaction_label.visible = false
	interaction_label.set_as_toplevel(true)
# warning-ignore:return_value_discarded
	PuzzleOverlay.connect("puzzle_mode_changed", self, "_on_puzzle_mode_changed")

func set_controllable(enabled):
	set_process(enabled)
	set_physics_process(enabled)
	set_process_unhandled_input(enabled)
	set_process_unhandled_key_input(enabled)
	set_process_input(enabled)

func _on_puzzle_mode_changed(enabled):
	set_controllable(not enabled)

func _process(delta : float):
	look_angle = get_global_mouse_position().angle_to_point(global_position)
	rotation = lerp_angle(rotation, look_angle, look_speed * delta)
	if velocity.length_squared() > 0.1:
		idle_walk.travel('walk')
	else:
		idle_walk.travel('idle')
	interaction_label.rect_position = interaction_label_pos.global_position

func _physics_process(delta : float):
	var input_direction = Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		Input.get_action_strength("move_up") - Input.get_action_strength("move_down")
	).normalized()
	var target_velocity = input_direction.y * global_transform.x * forward_speed + input_direction.x * global_transform.y * sideways_speed

	var accel = acceleration if target_velocity != Vector2.ZERO else deceleration
	velocity = velocity.move_toward(target_velocity, accel * delta)

	velocity = move_and_slide(velocity)

func _unhandled_input(event):
	if event.is_action_pressed("interact") and not event.is_echo():
		if interactable:
			animation_tree['parameters/interact/active'] = true
			interactable.interact()

func _on_InteractionArea2D_area_entered(area):
	interactable = area
	interaction_label.visible = true

func _on_InteractionArea2D_area_exited(_area):
	interactable = null
	interaction_label.visible = false
