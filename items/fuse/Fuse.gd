extends Area2D


func _ready() -> void:
	position = position.round()
	rotation = randf() * TAU


func _on_Fuse_body_entered(body: Node) -> void:
	if not body is Player:
		return
	Global.inventory["Fuse"] += 1
	queue_free()
