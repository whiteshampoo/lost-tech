extends Area2D

export(PackedScene) var puzzle : PackedScene

func interact():
	PuzzleOverlay.show_puzzle(puzzle)
