extends Control

onready var Anim : AnimationPlayer = $Animation

export (String, FILE) var menu : String = ""


func _ready() -> void:
	assert(menu, "No Menu-Scene selected")
	Anim.play("Intro")


func _input(event: InputEvent) -> void:
	if event.is_action("skip_intro"):
		goto_menu()


func goto_menu() -> void:
	Global.change_scene(menu)
