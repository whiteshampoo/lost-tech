extends Control


func _on_Back_pressed() -> void:
	Global.change_scene("res://ui/mainmenu/MainMenu.tscn")


func _on_RichTextLabel_meta_clicked(meta : String) -> void:
# warning-ignore:return_value_discarded
	OS.shell_open(meta)
