extends Control


onready var Fullscreen : Button = $Center/Container/Fullscreen
onready var Exit : Button = $Center/Container/Exit
onready var Level : SpinBox = $Center/Container/Container/Level

func init() -> void:
	print("came from level ", Global.playing)
	if Global.playing:
# warning-ignore:narrowing_conversion
		Global.level = max(Global.level, Global.playing)
		Global.playing += 1
		if Global.playing > count_puzzle():
			print("last level")
			Global.playing = 0
			Global.change_scene("res://ui/mainmenu/Credits.tscn")
			return
		
		print("goto level ", Global.playing)
		start_level(Global.playing)
		

func _ready() -> void:
	init()
	
	Fullscreen.pressed = OS.window_fullscreen
	if OS.get_name() == "HTML5":
		Exit.queue_free()
	Level.max_value = min(count_puzzle(), Global.level + 1)
	Level.value = Global.level + 1
	
	


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("fullscreen"):
		Fullscreen.pressed = OS.window_fullscreen

func start_level(puzzle : int) -> void:
	Global.playing = puzzle
	print(Global.level, Global.playing)
	Global.change_scene("res://assets/puzzle/Puzzle_%d.tscn" % puzzle)
	visible = false

func count_puzzle() -> int:
	var count : int = 0
	var dir : Directory = Directory.new()
# warning-ignore:return_value_discarded
	dir.open("res://assets/puzzle")
# warning-ignore:return_value_discarded
	dir.list_dir_begin()
	while true:
		var file : String = dir.get_next()
		if not file:
			break
		if file.begins_with("Puzzle_"):
			count += 1
# warning-ignore:return_value_discarded
	dir.list_dir_begin()
	return count


func _on_Exit_pressed() -> void:
	get_tree().quit()


func _on_Start_pressed() -> void:
# warning-ignore:narrowing_conversion
	start_level(Level.value)


func _on_Fullscreen_toggled(button_pressed: bool) -> void:
	OS.window_fullscreen = button_pressed


func _on_Credits_pressed() -> void:
	Global.change_scene("res://ui/mainmenu/Credits.tscn")
