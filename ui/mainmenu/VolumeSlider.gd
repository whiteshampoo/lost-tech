extends HBoxContainer
tool

onready var Text : Label = $Text
onready var Volume : HSlider = $Volume
onready var Mute : Button = $Mute

export (String, "Master", "Sound", "Music") var bus : String = "Master"
export var label_text : String = "Replace Me" setget set_label_text


func set_label_text(text : String) -> void:
	label_text = text
	if is_instance_valid(Text):
		Text.text = label_text + ":"


func _ready() -> void:
	set_label_text(label_text)
	if not Engine.editor_hint:
		audio_update_mute(bus, Global.audio_get_mute(bus))
		audio_update_volume(bus, Global.audio_get_volume(bus))


func disconnect_all(signals : Array) -> Array:
	var out : Array = Array()
	for con in get_incoming_connections():
		if con.signal_name in signals:
			out.append(con)
			con["source"].disconnect(con["signal_name"], self, con["method_name"])
	return out


func connect_all(signals : Array) -> void:
	for con in signals:
		con["source"].connect(con["signal_name"], self, con["method_name"])
		

func audio_update_mute(_bus : String, mute : bool) -> void:
	if not bus == _bus:
		return
	var signals : Array = disconnect_all(["value_changed", "toggled"])
	Volume.editable = not mute
	Mute.pressed = mute
	connect_all(signals)


func audio_update_volume(_bus : String, linear : float) -> void:
	if not bus == _bus:
		return
	var signals : Array = disconnect_all(["value_changed"])
	Volume.value = linear
	connect_all(signals)



func _on_Mute_toggled(button_pressed: bool) -> void:
	Global.audio_set_mute(bus, button_pressed)


func _on_Slider_value_changed(value: float) -> void:
	Global.audio_set_volume(bus, value)
