extends CanvasLayer

signal puzzle_mode_changed(enabled)

onready var V3D_Container : ViewportContainer = $ViewportContainer
onready var V3D : Viewport = $ViewportContainer/PuzzleViewport

var puzzle_mode : bool setget set_puzzle_mode

func _ready():
	self.puzzle_mode = false

func set_puzzle_mode(enabled : bool) -> void:
	puzzle_mode = enabled
	if puzzle_mode:
		V3D_Container.visible = true
		yield(get_tree(),"idle_frame") # don't take input from current frame
		V3D.gui_disable_input = false
	else:
		V3D_Container.visible = false
		V3D.gui_disable_input = true
	emit_signal("puzzle_mode_changed", puzzle_mode)

func show_puzzle(puzzle : PackedScene) -> void:
	self.puzzle_mode = true
	for p in V3D.get_children():
		p.queue_free()
	V3D.add_child(puzzle.instance())

