extends Viewport

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("interact") and not event.is_echo():
		PuzzleOverlay.puzzle_mode = false
