extends CanvasLayer

onready var LabelAmount : Label = $Fuses/LabelAmount
onready var Menu : ColorRect = $Menu
onready var Fullscreen : Button = $Menu/Center/Container/Fullscreen
onready var Quit : Button = $Menu/Center/Container/Quit

func _ready() -> void:
	if OS.get_name() == "HTML5":
		Quit.queue_free()

func _process(_delta: float) -> void:
	LabelAmount.text = str(Global.inventory["Fuse"])
	if Input.is_action_just_pressed("pause"):
		pause(not get_tree().paused)
	if Input.is_action_just_pressed("fullscreen"):
		Fullscreen.pressed = OS.window_fullscreen


func pause(paused : bool) -> void:
	get_tree().paused = paused
	Menu.visible = paused

func _on_Continue_pressed() -> void:
	pause(false)


func _on_Menu_pressed() -> void:
	get_tree().paused = false
	Global.playing = 0
	Global.change_scene("res://ui/mainmenu/MainMenu.tscn")


func _on_Quit_pressed() -> void:
	get_tree().quit()


func _on_Fullscreen_toggled(button_pressed: bool) -> void:
	OS.window_fullscreen = button_pressed
